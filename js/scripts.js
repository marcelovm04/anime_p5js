var dinos = []
var N = 10
var cont = 0
var cont_frames = 0
var delay = 10

var x = 0
var y = 0

function setup() {
    createCanvas(1600, 800)
    for(let i = 1; i <= N; i++) {
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }
}

function draw() {
    cont_frames++
    if (!(cont_frames % delay) ){
        background(100)
    
        let img = dinos[cont]
        image(img, 10+cont_frames, 10, img.width/2, img.height/2)
    
        cont++
        if(cont >= N) {
            cont = 0
        }
        if (cont_frames >= 1600) {
            cont_frames = 0
        }
    }

}
